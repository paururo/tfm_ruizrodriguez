#!/bin/bash

#Usage # sbatch [this script]
#Name of the job
#SBATCH --job-name=rmat
#Runtime of the job - one day should be sufficient for datasets with up to 1000 genomes & 32 cores

#SBATCH --time=7-00:00:00
#Define sdout path
#SBATCH --output=/home/ruizro/proyectos_paula/tb_epitopes/deletions/pca/selection2.o
#Define sderr path
#SBATCH --error=/home/ruizro/proyectos_paula/tb_epitopes/deletions/pca/selection2.e
#Define the queue (Quality Of Service) to which the task shall be submitted to
#SBATCH --nodes=1
#SBATCH --partition=long
#SBATCH --mem=100000
#SBATCH --cpus-per-task=10
module load R
Rscript bin_pca.R
