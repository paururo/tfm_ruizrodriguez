#!/bin/bash
#Usage # sbatch [this script]
#Name of the job
#SBATCH --job-name=tbTree
#Runtime of the job - one day should be sufficient for datasets with up to 1000 genomes & 32 cores

#SBATCH --time=15-00:00:00
#Define sdout path
#SBATCH --output=/home/ruizro/proyectos_paula/big_treeTB/tree.o
#Define sderr path
#SBATCH --error=/home/ruizro/proyectos_paula/big_treeTB/tree.e
#Define the queue (Quality Of Service) to which the task shall be submitted to
#SBATCH --nodes=1
#SBATCH --partition=long
#SBATCH --mem=500000
#SBATCH --cpus-per-task=13

#FastTree -gtr -nt snps.ess.nodr.fas > tb.tree

#raxmlHPC-AVX -p 12857 -s snps.ess.nodr.fas -m GTRCAT --asc-corr=lewis -n all.txt

iqtree2 -s snps.ess.nodr.fas.reduced -B 1000 -m GTR -nt AUTO
