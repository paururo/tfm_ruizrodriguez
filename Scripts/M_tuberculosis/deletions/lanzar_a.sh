#!/bin/bash

#Name of the job
#SBATCH --job-name=deletions

#Define how many cores to use
#SBATCH --ntasks=20

#How much memery to use per core
#SBATCH --mem-per-cpu=5G

#Runtime of the job - one day should be sufficient for datasets with up to 1000 genomes & 32 cores


#SBATCH --time=7-00:00:00
#Define sdout path
#SBATCH --output=/home/ruizro/proyectos_paula/tb_epitopes/deletions/tcell/A.out
#Define sderr path
#SBATCH --error=/home/ruizro/proyectos_paula/tb_epitopes/deletions/tcell/A.error
#Define the queue (Quality Of Service) to which the task shall be submitted to
#SBATCH --nodes=1
#SBATCH --partition=medium

module load python/3.8


python3 nuevo_coordenadas.py -f L2.num -d '/storage/PGO/data/mtb/mappings/v1/' -c annotated_epitopes.txt -n epitopesT_L2
