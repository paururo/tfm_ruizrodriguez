#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import subprocess
import argparse

def read_fileGnumbers(fichero):
    """
    Funcion para procesar de un fichero con cada gnumber en una linea y obtener una lista de gnumbers
    """
    lista_gnumbers = list() # inicializar lista 
    with open(fichero,"r") as g_file: #abrir el fichero para leer
        for gnumber in g_file: #recorrer cada linea
            if "G" in gnumber: #asegurar que es gnumber
                gnumber = gnumber.replace("\n","") #eliminar saltos de linea
                lista_gnumbers.append(gnumber) #anyadir los gnumbers a la lista
    return lista_gnumbers #devuelvo la lista gnumbers
        
def generate_deletions_file(gnumbers_list, ruta_original, directorio_script, fichero, nombre):
    for gnumber in gnumbers_list:
        g_gnumber = gnumber.split("_")[0]
        l_gnumber = gnumber.split("_")[1]
        ruta = ruta_original + g_gnumber[0:3] + '/' + g_gnumber[3:5] + '/' + g_gnumber[5] + '/'
        subprocess.call("""perl """ + """findGeneDeletions2.pl """ + ruta + g_gnumber + ".coverage.all.pos " + fichero + " 0 >> " + nombre + ".deletions", shell = True)
        subprocess.call("""perl -p -i -e 's/""" + g_gnumber + """/"""+gnumber+"\t"+l_gnumber+"""/g' """+nombre+".deletions", shell = True)

def main():
    directorio_script = os.getcwd()
    parser = argparse.ArgumentParser(description = 'script to get snps of interest, for each gnumber') #paso variables por argumentos
    parser.add_argument('-f', dest = 'file', help = 'File with numbers.', required = True)
    parser.add_argument('-d', dest = 'directory', help = 'Directory where G numbers are stored.', required = True)
    parser.add_argument('-n', dest = 'name', help = 'Name of the output files.') # nombre para la salida del output
    parser.add_argument('-c', dest = 'coordinates', help = 'File with genomic coordinates of genes of interest.') #fichero con las coordenadas a mirar
    
    args = parser.parse_args()
    ruta_numb = args.directory
    gnumbers_list = read_fileGnumbers(args.file)
    generate_deletions_file(gnumbers_list,ruta_numb,directorio_script,args.coordinates,args.name)


if __name__ == "__main__":
    #print(__doc__) #imprimimos la documentacion
    main()
    
    
