#!/bin/bash
#Usage # sbatch [this script]
#Name of the job
#SBATCH --job-name=random
#Runtime of the job - one day should be sufficient for datasets with up to 1000 genomes & 32 cores

#SBATCH --time=15-00:00:00
#Define sdout path
#SBATCH --output=/home/ruizro/random/gen_out.o
#Define sderr path
#SBATCH --error=/home/ruizro/random/gen_out.e
#Define the queue (Quality Of Service) to which the task shall be submitted to
#SBATCH --nodes=1
#SBATCH --partition=long
#SBATCH --mem=900000
#SBATCH --cpus-per-task=60
module load python/3.8
time python3 randomize_epitopes.py -f 1561tcell.txt -del coordinates2exclude -rep 100 -mut syn.txt-nonsyn.txt
