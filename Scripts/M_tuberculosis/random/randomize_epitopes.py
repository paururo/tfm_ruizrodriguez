#!/usr/bin/env python3
# -*- coding: utf-8 -*-  


""" 

"""                                                                               
from random import seed
from random import randint
import os,subprocess,argparse

def leer_y_crearlista(entrada):
  """
  Funcion para leer un archivo sacando las dos posiciones de cada linea,
  crear de estas dos una lista, y esta meterla en otra lista.
  """
  lista_pos = [] #Creamos una lista vacia
  with open(entrada,'r') as input_arch:
      for line in input_arch: #leemos linea por linea.
        line = line.split()
        mini_lista = []
        Posicion1 = int(line[0])
        Posicion2 = int(line[1])
        mini_lista.append(Posicion1)
        mini_lista.append(Posicion2)
        lista_pos.append(mini_lista)
  return lista_pos

def descomponrange (list):
  """
  Funcion para sacar el rango de los 2 numeros de una lista
  """
  primerElmt = 0
  for i in list:
    if primerElmt == 0:
      primerElmt = i
    else:
      segundoElmt = i
  return range(primerElmt,segundoElmt)

def comparten(x,y):
  """
  Funcion con dos rangos para ver si se solapan.
  si lo comparten la funcion sera True, sino False
  """
  z = [i for i in x if i in y]
  if len(z) == 0:
    return False
  else:
    return True

def busqueda(listaDeposiciones,range_minilista):
  """
  Funcion que llama a otras, para ver los numeros 
  no coinciden con ninguno de la lista dada. Si coincide
  se rompe, y todosFalsos no sera igual a la long de la lista dada.
  Entonces se busca un nuevo numero. 
  """
  todosFalsos = 0
  for sublist in listaDeposiciones: 
    range_sublist = descomponrange(sublist)
    compara = comparten(range_minilista,range_sublist)
    if compara is False:
      todosFalsos = todosFalsos+1
    elif compara is True:
      break
  return todosFalsos

def crear_salida(entrada,nombreArch, fichero):
  """
  Funcion que convierte la lista de listas en un output
  de dos columnas y linea por lista en un fichero de 
  texto generado de novo.
  """
  nombre_arch = str(fichero[0:-4])+str(nombreArch)+".txt"
  abrir_ = open(nombre_arch,"w")
  abrir_.write("Archivo con las nuevas coordenadas:\n")
  abrir_.close()
  string_lista = map(str, entrada) 
  for i in string_lista:
    letra = ''.join(i)
    camb_1 = letra.replace(", ","\t")
    camb_2 = camb_1.replace("[","")
    camb_3 = camb_2.replace("]","")
    abre = open(nombre_arch,"a")
    abre.write("\n" + camb_3)
    abre.close()

if __name__=='__main__':
  print(__doc__)
  parser = argparse.ArgumentParser(description='')
  parser.add_argument('-f',dest='entrada',help = 'file',required=True)
  parser.add_argument('-del',dest='dele',help = '',required=True)
  parser.add_argument('-rep',dest='replicas',help = '',required=True)
  parser.add_argument('-mut',dest='mutaciones',help = '',required=True)
  argumento = parser.parse_args()
  mutacion1 = argumento.mutaciones
  condicion = False
  #####################movida para crear los archivos contadores cuando son mas de uno
  if "-" in mutacion1:
    intromut = mutacion1.strip().split("-")
    for i in range(0,len(intromut)):
      contadores1 = open("contadores"+str(i+1)+".txt","w")
      contadores1.close()
    condicion = True
  else:
    contadores1 = open("contadores1.txt","w")
    intromut = list(mutacion1)
  #############################################################################
  print("Informacion:\nTrabajaremos en el mismo directorio donde se encuentra el script.\n\n\tEl archivo de entrada tiene que tener dos columnas \n\tdonde la primera posicion es menor que la segunda, \n\testas dos columnas estara separadas por un tabulador")
  arch_entrada = argumento.entrada
  print("El archivo de entrada sera:", arch_entrada)
  arch_entrada_del = argumento.dele
  salida = open(arch_entrada[:-4]+"_" +str(argumento.replicas)+".txt","w")
  #########movida para hacer las replicas, generar epitopos random
  for prueba in range(0,int(argumento.replicas)):
    salida.write("Replica_" + str(prueba+1)+"\n")
    lista_pos = leer_y_crearlista(arch_entrada)
    lista_del = leer_y_crearlista(arch_entrada_del)
    posiciones_buenas = []
    for epitopo in lista_pos:
      repetir = 0
      while repetir == 0:
        nuevas_pos = []
        posicion1= int(epitopo[0])
        posicion2= int(epitopo[1])
        aleatorio = randint(0,50000)
        nueva_pos1 = posicion1 + aleatorio
        nueva_pos2 = posicion2 + aleatorio
        nuevas_pos.append(nueva_pos1)
        nuevas_pos.append(nueva_pos2)
        range_pos = descomponrange(nuevas_pos)
        comprobar_propia_lista = busqueda(lista_pos, range_pos)
        comprobar_lista_del = busqueda(lista_del,range_pos)
        if comprobar_propia_lista == len(lista_pos) and comprobar_lista_del == len(lista_del):
          posiciones_buenas.append(nuevas_pos)
          repetir = 1
    inside = open("new_epitopes.txt","w")
    for i in posiciones_buenas:
      sentencia =  str(i[0]) +"\t"+ str(i[1]) + "\n"
      salida.write(sentencia)
      inside.write(sentencia)
      ##en el fichero de los intervalos las posiciones estan en las columnas 1,2 la mutacion esta en la columna 1
    inside.close()
  #############################################################################################################################################
  ##movida para poner las mutaciones en los intervalos
    lista_por_salidas=[]
    for i in intromut:
      subprocess.call("""awk 'NR==FNR{a[NR]=$1; b[NR]=$2; c[NR] = $5; z=NR; next} {j=0; i=1; r=""; while (i<=z){if (($1 <= a[i]) && ($2 >= a[i])) {j++; r=r"\t"b[i]"-"a[i]} i++}{print j"\t"$0"\t" r}}' """+ i +" new_epitopes.txt > "+i[:-4]+"salida.txt", shell = True)
      lista_por_salidas.append(i[:-4]+"salida.txt")
    lista_numeros = []
    cuenta = 1
    lista_contadores = []
    for i in lista_por_salidas:
      with open(i,'r') as input_sal:
        for line in input_sal:
          numero = line.split("\t")[0]
          if numero not in lista_numeros and numero != '':
            lista_numeros.append(numero)
      for num in lista_numeros:
        contador = 0
        with open(i,'r') as input_sal:
          for line in input_sal:
            numero = line.split("\t")[0]
            if numero == num:
              contador += 1
        contadores1=open("contadores"+str(cuenta)+".txt","a")
        contadores1.write(num + "\t" + str(contador)+"\n")
        contadores1.close()
      lista_contadores.append("contadores"+str(cuenta)+".txt")
      cuenta += 1

  for i in range(0,len(lista_contadores)):
    lista_numeros = []
    with open("average"+ str(i+1)+".txt",'w') as average:
      titulo = "mutations\tepitopes\taverage\n"
      average.write(titulo)
      with open(lista_contadores[i],'r') as input_sal:
        for line in input_sal:
          numero = line.split("\t")[0]
          if numero not in lista_numeros:
            lista_numeros.append(numero)
      for num in lista_numeros:
        contador = 0
        with open(lista_contadores[i],'r') as input_sal:
          for line in input_sal:
            numero = line.split("\t")[0]
            if numero == num:
              contador += int(line.split("\t")[1])
        string = str(num)+ "\t" + str(contador) + "\t" + str(contador/int(argumento.replicas)) + "\n"
        average.write(string)
  salida.close()
  subprocess.call("""rm *salida.txt """, shell = True)
  subprocess.call("""rm new_epitopes.txt """, shell = True)
  lista_ficheros=[]
  for i in range(0,len(intromut)):
    contador="contadores"+str(i+1)+".txt"
    averg="average"+str(i+1)+".txt"
    nom_sal="contador_"+ intromut[i]
    nom_av="average_"+ intromut[i]
    os.rename(contador,nom_sal)
    os.rename(averg,nom_av)
    lista_ficheros.append(nom_sal)
  


  ########################################
  for ficha in lista_ficheros:
    nombresal = "salida_"+ ficha

    fichero = open(ficha,"r")
    numeros = []
    for line in fichero:
        linea = line.split("\t")
        if linea[0] in numeros:
            pass
        else:
            numeros.append(linea[0])
    maximo = int(max(numeros))
    fichero.close()

    print(maximo)

    fichero = open(ficha,"r")
    lista_dicionarios=[]
    lista=[]
    contador=0
    for line in fichero:
        line = line.replace("\n","")
        linea = line.split("\t")
        contador+=int(linea[1])    
        if contador<1226:
            lista.append(linea)

        if contador == 1226:
            lista.append(linea)
            contador=0

            dictio={}
            for elemento in lista:
                dictio[elemento[0]] = elemento[1]
            lista_dicionarios.append(dictio)        
            lista=[]
    fichero.close()

    MAX_DIGITS = 10
    def _keyify(x):
        try:
            xi = int(x)
        except ValueError:
            return 'S{0}'.format(x)
        else:
            return 'I{0:0{1}}'.format(xi, MAX_DIGITS)
    salida=open(nombresal,"w")
    cadena1=''
    for l in range(0,len(numeros)):
        cadena1 += "mut_"+str(l)+"\t" 
    cadena2 = cadena1+"\n"
    salida.write(cadena2)
    lista_grande = []

    for dictio in lista_dicionarios:
        cadena=''
        longitud=len(dictio)
        lista_media = []
        for k in sorted(dictio, key=_keyify):
            lista=[]
            cadena += k+"\t"+dictio[k]+"\t"
            lista.append(k)
            lista.append(dictio[k])
            lista_media.append(lista)
        lista_grande.append(lista_media)
    for replica in lista_grande:
        long=len(replica)
        contador = 0
        cadena = ''
        elemento_antes=0
        for elementos in replica:
            if contador == int(elementos[0]):
                cadena += elementos[1]+"\t"
                elemento_antes=int(elementos[0])
            if contador < int(elementos[0]):
                if int(elementos[0]) - elemento_antes == 1:
                    cadena2 = ''
                else:
                    veces = int(elementos[0]) - int(elemento_antes) - 1
                    cadena2 = ("0"+"\t")*veces
                cadena += cadena2 + str(elementos[1])+"\t"
                elemento_antes=int(elementos[0])
            if contador+1 == long:
                veces= maximo-int(elementos[0]) 
                print("veces",veces)
                cadena2 = ("0"+"\t")*veces
                cadena += cadena2
            contador+=1
        cadena+="\n"
        salida.write(cadena)
    print(maximo)

    salida.close()

    
