# Scripts for GWAS analysis

La carpeta contiene una serie de scripts en python, bash y R. Para realizar el analisis de secuencias de SARS COV 2.
El analisis parte de un vcf que se genera con snp-sites con la opcion -v. en el fasta que se analiza para formar el VCF
tiene que tener la referencia en la primera secuencia.

gwas_pipe.sh : script de bash con todos los pasos a realizar requiere los argumentos
		-h,	prints this help
		-v,	vcf with data to analyze, output from snp sites
		-n,	name some output
		-p,	phenotype file
		-m,	file with pca analysis
        -w, working directory

crea_matriz.py: script para transformar el fichero vcf en una matriz de nucleotidos
transformar.R: script para tratar las posiciones ambiguas y duplicar esa posicion
crea_pheno.py: script para tratar la informacion fenotipica y darle el formato para plink