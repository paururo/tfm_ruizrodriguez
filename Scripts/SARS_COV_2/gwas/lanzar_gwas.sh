#!/bin/bash
#Name of the job
#SBATCH --job-name=gwas_analysis

#Define how many cores to use
#SBATCH --ntasks=12

#How much memery to use per core
#SBATCH --mem-per-cpu=8G

#Runtime of the job - one day should be sufficient for datasets with up to 1000 genomes & 32 cores


#SBATCH --time=7-00:00:00
#Define sdout path
#SBATCH --output=/home/ruizro/proyectos_paula/scripts/sarscov-2/gwas/pruebas/gl_aln2.out
#Define sderr path
#SBATCH --error=/home/ruizro/proyectos_paula/scripts/sarscov-2/gwas/pruebas/gl_aln2.err
#Define the queue (Quality Of Service) to which the task shall be submitted to
#SBATCH --nodes=1
#SBATCH --partition=medium

#########Archivos que se necesitan
# ejecutable plink
# info_pheno.txt
# outMut2021-03-17.vcf
module load R
module load python/3.8
sh /home/ruizro/proyectos_paula/scripts/sarscov-2/gwas/gwas_pipe.sh -v prueba.vcf -n prueba -p info_pheno.txt -m mds2.txt -w /home/ruizro/proyectos_paula/scripts/sarscov-2/gwas/pruebas/
