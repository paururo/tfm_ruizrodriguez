#! /usr/bin/python3
# -*- coding: utf-8 -*-

import argparse

def transformar(fich_entrada, salida):
    fichero = open(fich_entrada,'r')
    salida = open(salida,'w')
    for line in fichero:
        linea = line.replace('\n','').split('\t')
        if "##" in line:
            a = "A"
        elif '#' in line:
            sentencia = "Coordinate/GNumber"+"\t"+"Gene"+"\t"+str(linea[9:]).replace('[','').replace(']','').replace("'",'').replace(", ",'\t')
            salida.write(sentencia)
        elif '#' not in line:
            lista = [int(i) for i in linea[9:]]
            lista3 = linea[3:4]
            n = str(linea[4:5]).replace('[','').replace(']','').replace("'",'').split(',')
            for i in n:
                lista3.append(i)
            lista2 = []
            cadena = "\n" + linea[1] + "\t" + "NA" + "\t"
            for i in lista:
                ente = int(i)
                cadena += lista3[ente] + "\t"
            value = (sum(lista2)*100)/(len(lista2)-1)
            escribe = str(int(linea[1]) + 54) + '\t' + str(value) + '\n'
            salida.write(cadena)
    salida.close()
    fichero.close()

def main():
    parser = argparse.ArgumentParser(description = 'script to transform vcf to genetic matrix') #paso variables por argumentos
    parser.add_argument('-i', dest = 'file', help = 'input file with vcf form') #fichero de entrada
    parser.add_argument('-o', dest = 'out', help = 'output name') #como se va a llamar el fichero de salida   
    args = parser.parse_args()
    transformar(args.file, args.out)

if __name__ == "__main__":
    #print(__doc__) #imprimimos la documentacion
    main()     