# CHESCK FOR ARGUMENTS

usage () {	echo "Usage gwas_pipe.sh 
		Example: ssh gwas_pipe.sh -v prueba.vcf -n prueba -p pheno.txt -m mds.txt -w directory
		
		-h,	prints this help
		-v,	vcf with data to analyze, output from snp sites
		-n,	name some output
		-p,	phenotype file
		-m,	file with pca analysis
    -w, working directory"

		exit 1 ; }

if [ $# -eq 0 ]; then
	usage;

else
	while getopts ":hv:n:p:m:w:" opt; do
	  case ${opt} in
	    h )
		usage;
	      ;;
	    v )
	      vcf=$OPTARG
	      ;;
	    n )
	      name=$OPTARG
	      ;;
	    p )
	      pheno=$OPTARG
	      ;;
	    m )
	      mds=$OPTARG
	      ;;
	    w )
	      work=$OPTARG
	      ;;
	    \? )
	      echo "Invalid option: $OPTARG. Write -h for more help" 1>&2
		exit 1
	      ;;
	    : )
	      echo "Invalid option: $OPTARG requires an argument. Write -h for more help" 1>&2
		exit 1
	      ;;
	  esac
	done

fi
cd $work

python3 /home/ruizro/proyectos_paula/scripts/sarscov-2/gwas/crea_matriz.py -i $vcf -o $name
Rscript --vanilla /home/ruizro/proyectos_paula/scripts/sarscov-2/gwas/transformar.R -f $name


cat  DYM_varSNPs.txt| sed s'/*/0/'g | sed s'/B/C/'g| sed s'/H/A/'g | sed s'/M\tM/A\tC/'g | sed s'/R\tR/A\tG/'g | sed s'/W\tW/A\tT/'g | sed s'/S\tS/C\tG/'g | sed s'/Y\tY/C\tT/'g | sed s'/K\tK/G\tT/'g | sed s'/K\tK/G\tT/'g| sed s'/K\tK/G\tT/'g > DYM_gwas_genotype.txt
grep -v -w "Gene"  DYM_gwas_genotype.txt > temp
cut -f2- temp > temp2 # se quita el cov
head -1 temp2 > posiciones.txt # saco los numeros de la primera linea
sed -i -e  s'/\t/\n/'g posiciones.txt # numeros en una linea a numero x linea
sed -i -e  s'/ //'g posiciones.txt # quito los espacios innecesarios
awk '{print 1,$0,0, $0}' posiciones.txt |awk '!NF || !seen[$0]++' > DYM_gwas.map # creo el fichero de las posiciones quitando las lineas redundantes
sed -i -e  s'/ /\t/'g DYM_gwas.map
rm posiciones.txt
sed -i -e '1,2d' temp2 #quitar las dos primeras lineas (posiciones y referencia)

python3 /home/ruizro/proyectos_paula/scripts/sarscov-2/gwas/crea_pheno.py -i $pheno

paste DYM_gwas_phenotype.txt temp2 > DYM_gwas.ped


#Eliminar ficheros
rm DYM_gwas_phenotype.txt
rm temp2
rm DYM_gwas_genotype.txt
rm temp
rm gl_aln.out

#Empieza con plink
plink  --file DYM_gwas --make-bed --geno .05 --out pfcorona --allow-no-sex

#Take out LD snps:
plink  --bfile pfcorona --indep-pairwise 50 5 0.8 -out pruned --allow-no-sex
plink  --bfile pfcorona --allow-no-sex --extract pruned.prune.in --out cleanpfcorona --make-bed --maf 0.001

plink --bfile cleanpfcorona --logistic --all-pheno --hwe 0.000005 --allow-no-sex --geno .05 --pheno pheno.txt --covar $mds --hide-covar --adjust --out salidafinal

#plink --allow-no-sex --bfile cleanpfcorona --covar c.txt --dummy-coding --write-covar
