#! /usr/bin/python3
# -*- coding: utf-8 -*-
import argparse

def procesar(name):
    fichero = open(name,'r')
    salida = open('DYM_gwas_phenotype.txt','w')
    salida2 = open('pheno.txt','w')
    titulo_salida2= "ID\tID\tPhenotype\n"
    salida2.write(titulo_salida2)
    for line in fichero:
        line=line.replace("\n", "").split("\t")
        sex = line[2].replace("Female","2").replace("Male","1").replace("NA","0")
        sentencia = line[0]+"\t"+line[0]+"\t0\t0\t"+sex+"\t"+line[1]+"\n"
        sentencia2 = line[0]+"\t"+line[0]+"\t"+line[1]+"\n"
        salida2.write(sentencia2)
        salida.write(sentencia)

    salida.close()
    fichero.close()    

def main():
    parser = argparse.ArgumentParser(description = 'script to transform vcf to genetic matrix') #paso variables por argumentos
    parser.add_argument('-i', dest = 'file', help = 'input file with vcf form') #fichero de entrada
    args = parser.parse_args()
    procesar(args.file)

if __name__ == "__main__":
    #print(__doc__) #imprimimos la documentacion
    main()     