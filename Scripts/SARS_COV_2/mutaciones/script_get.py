#! /usr/bin/python3
# -*- coding: utf-8 -*-
import argparse
import subprocess
import os 


with open("lin2.fa","r") as entrada:
    contador = 0
    for line in entrada:
        line = line.replace("\n","").split("\t")
        if contador == 0:
            refe = line
        else:
            with open ("salida.fasta","w") as salida:
                escribe=refe[0]+"\n"+refe[1]+"\n"+line[0]+"\n"+line[1] 
                salida.write(escribe)
            #subprocess.call("seqkit seq -w 60 salida.fasta > alignment.msf", shell = True)
            #subprocess.call("java -jar /home/ruizro/programas/jvarkit/dist/msa2vcf.jar --consensus 'MN908947.3' --output p.vcf alignment.msf", shell = True)   
            #subprocess.call("grep '>' salida.fasta | wc -l", shell = True)
            os.system("java -jar /home/ruizro/programas/jvarkit/dist/msa2vcf.jar salida.fasta --consensus 'MN908947.3'  --output p.vcf ")    
            with open("final.vcf","a") as writ:
                with open("p.vcf","r") as ea:
                    for line in ea:
                        if "#" not in line:
                            writ.write(line)
        contador += 1
