#!/bin/bash
#Name of the job
#SBATCH --job-name=cvd_all

#Define how many cores to use
#SBATCH --ntasks=64

#How much memery to use per core
#SBATCH --mem-per-cpu=10G

#Runtime of the job - one day should be sufficient for datasets with up to 1000 genomes & 32 cores


#SBATCH --time=7-00:00:00
#Define sdout path
#SBATCH --output=/home/ruizro/iqtree2/contreeall21.out
#Define sderr path
#SBATCH --error=/home/ruizro/iqtree2/contreeall21.error
#Define the queue (Quality Of Service) to which the task shall be submitted to
#SBATCH --nodes=1
#SBATCH --partition=long

#/home/ruizro/iqtree2/iqtree2 -s all_6099.fasta -B 1000 -czb -m GTR -nt AUTO
/home/ruizro/iqtree2/iqtree2 -s mask_longdataset100.fa -B 1000 -m GTR -nt AUTO
/home/ruizro/iqtree2/iqtree2 -s mask_longdataset500.fa -B 1000 -m GTR -nt AUTO
#/home/ruizro/iqtree2/iqtree2 -s aln.brote.fasta -B 1000 -m GTR -nt AUTO
#/home/ruizro/iqtree2/iqtree2 -s aln.linajes.fasta -B 1000 -m GTR -nt AUTO
#/home/ruizro/iqtree2/iqtree2 -s aln.longcovid.fasta -B 1000 -m GTR -nt AUTO
