#! /usr/bin/python3
# -*- coding: utf-8 -*-
"""
       _                          _       
   ___| | ___  __ _ _ __     __ _| |_ __  
  / __| |/ _ \/ _` | '_ \   / _` | | '_ \ 
 | (__| |  __/ (_| | | | | | (_| | | | | |
  \___|_|\___|\__,_|_| |_|  \__,_|_|_| |_|
                                          
Script for clean an alingment and eliminate sequences with more than 5% Ns 
and more than 1000 gaps. Designed for SARS-CoV-2 data.
Command:
    python3 clean_aln.py -f input.fasta -n sarscov210730
Outputs:
    name + _clean.fa -> filtered fasta
    name + .stats -> number of gaps and percentage of Ns
    name + _lin.fa -> linearized fasta  

Contact -> @paururo
"""

__author__ = "Paula Ruiz Rodriguez"
__credits__ = ["Paula Ruiz Rodriguez"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Paula Ruiz Rodriguez: @paururo, @ruizro"
__email__ = "paula.ruiz-rodriguez@uv.es"
__status__ = "Finished"

import os
import argparse
import sys

def count(line, statswrite, fastawrite):
    """
    Function for count number of Ns and gaps, write this info into stats file,
    and generate a new normal fasta. And filter by sequences with less than 5%
    of Ns and more than 1000 gaps
    Inputs:
        line        -> structure id + "\t + sequence + "\n"
        statswrite  -> file for write stats: gaps and Ns
        fastawrite  -> multifasta with clean sequences
    """
    ids = line.replace("\n","").replace(">","").split("\t")[0]
    seq = line.replace("\n","").split("\t")[1]
    Ns = seq.count("N")/29903*100
    gaps = seq.count("-")
    condition = False
    sentence = ids + "\t" + str(Ns) + "\t" + str(gaps) + "\n"
    statswrite.write(sentence)
    if Ns < 5:
        condition = True
        if gaps >= 1000:
            condition = False
    if condition:
        fasta = ">" + ids + "\n" + seq + "\n"
        fastawrite.write(fasta)

def linearize(fasta, name):
    """
    Function for linearize normal multifasta into id + \t + sequence,
    with awk command.
        Inputs:
            fasta           ->  normal fasta file
        Outputs:
            name + _lin.fa  ->  linearize fasta file 
    """

    bashCommand = """awk '/^>/ {printf("%s%s\\t",(N>0?"\\n":""),$0);N++;next;} {printf("%s",$0);} END {printf("\\n");}' """ \
        + fasta + " > " + name + "_lin.fa"
    os.system(bashCommand)

def stats(fasta, name):
    """
    Function for generate a linearize fasta, generate stats file, and clean 
    multifasta file.
    """
    linearize(fasta, name)
    with open(name + ".stats", "w") as statswrite:
        header = "#id\tNs\tgaps\n"
        statswrite.write(header)
        with open(name + "_clean.fa","w") as fastawrite:
            with open(name + "_lin.fa","r") as entrada:
                for line in entrada:
                    count(line, statswrite, fastawrite)

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-f", "--fasta", help="Input multifasta aln", type=str, required=True)
    parser.add_argument("-n", "--name", help="Name for output", type=str, required=True)

    args = parser.parse_args()

    stats(args.fasta, args.name)

if __name__ == "__main__":
    print(__doc__)
    print(__email__)
    print("Version:",__version__)
    main()
