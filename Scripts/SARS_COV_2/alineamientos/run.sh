#!/bin/bash

#SBATCH --job-name=dataset
#SBATCH --partition=long
#SBATCH --mem=150gb
#SBATCH --cpus-per-task=64


usage () {
    echo "Usage: $0 -i INPUT_DIR -o OUTPUT_DIR [-h]"
}

logstr () {
    echo "$(date) | $@"
}

# Paths
if [ ! -z $SLURM_JOB_ID ];  then
    # slurm (check original location)
    SCRIPT_PATH="$(scontrol show job $SLURM_JOB_ID | awk -F= '/Command=/{print $2}' | cut -d " " -f1)"
    BASE_DIR="$(realpath "$(dirname $SCRIPT_PATH)/..")"
else
    # not slurm
    BASE_DIR="$(realpath "$(dirname $0)/..")"
fi
CLEAN_PY="$BASE_DIR/common/clean_aln.py"
REF_FASTA="$BASE_DIR/common/reference.fasta"

# Get arguments
while getopts "hi:o:" option
 do
  case "${option}" in
    h) usage; exit 1;;
    o) OUTDIR=$OPTARG;;
    i) INDIR=$OPTARG;;
	esac
done
shift "$(($OPTIND -1))"

if [ -z $OUTDIR ] || [ -z $INDIR ]; then
    usage
    echo "Arguments -i and -o must be provided"
    exit 1
fi

logstr "OUTDIR: $OUTDIR"
logstr "INDIR:  $INDIR"
mkdir -p "$OUTDIR"


# === MAIN ===

# Decompress metadata (contains metadata.csv and readme.txt)
logstr "Decompressing metadata"
metadata_xz="$(ls "$INDIR"/*globalmetadata.tar.xz)"
# unxz --threads=0 -k $metadata_xz
tar xf $metadata_xz --directory "$INDIR"
mv "$INDIR/metadata.tsv" "$OUTDIR"
rm "$INDIR/readme.txt"

# Decompress sequences (contains sequences.fasta and readme.txt)
logstr "Decompressing sequences"
sequences_xz="$(ls "$INDIR"/*globalsequences.tar.xz)"
# unxz --threads=0 -k $sequences_xz
tar xf $sequences_xz --directory "$INDIR"
mv "$INDIR/sequences.fasta" "$OUTDIR"
rm "$INDIR/readme.txt"

# Get dataset name from sequences file
bn="$(basename "$sequences_xz")"
arr=(${bn//_/ })
dataset_name="${arr[0]}"

# Align with Nextalign
logstr "Aligning sequences"
nextalign \
    --sequences="$OUTDIR/sequences.fasta" \
    --reference="$REF_FASTA" \
    --output-dir="$OUTDIR/nextalign" \
    --output-basename="$dataset_name"

# Clean alignment
logstr "Cleaning alignment"
mkdir -p "$OUTDIR/results"
python3 $CLEAN_PY \
    -f "$OUTDIR/nextalign/$dataset_name.aligned.fasta" \
    -n "$OUTDIR/results/$dataset_name"

logstr "Done!"
