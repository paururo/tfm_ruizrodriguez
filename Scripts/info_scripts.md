📦Scripts  
 ┣ 📂M_tuberculosis: carpeta con todos los scripts usados para _M. tuberculosis_  
 ┃ ┣ 📂deletions: carpeta para el analisis de los epitopos delecionados mediante el coverage  
 ┃ ┃ ┣ 📜lanzar_a.sh: script de bash para ejecutar el script de python en el sistema de colas  
 ┃ ┃ ┗ 📜nuevo_coordenadas.py: script de python mediante un fichero con el nombre de los genomas a analizar(-f), con el directorio donde esta el coverage(-d), con los epitopos a analizar (c) y con un nombre del trabajo a lanzar (-n)  
 ┃ ┣ 📂dnds: carpeta para el análisis de dnds    
 ┃ ┃ ┣ 📜kaksTcellsVersion_1.R: script de R para calcular el dnds para cada linaje, con un alineamiento fasta como input  
 ┃ ┃ ┣ 📜random_analysis.Rmd: script para calcular los valores randomizados de dN, con un alineamiento fasta como input  
 ┃ ┃ ┗ 📜WilcoxondN.Rmd: script para hacer los calculos estadisticos entre los dN  
 ┃ ┣ 📂pca_deletions: directorio para analizar los epitopos delecionados mediante un PCA  
 ┃ ┃ ┣ 📜lanzar_r.sh: script para lanzar en el sistema de colas el analisis PCA  
 ┃ ┃ ┗ 📜proces_pca.R : script de R para el calculo del PCA mediante un dataframe de epitopos delecionados en cada genoma  
 ┃ ┣ 📂plot_esx: directorio para plotear las regiones genicas de esx-1  
 ┃ ┃ ┣ 📜esx1_matrix.gene: dataframe con la informacion de cada gen de esx1  
 ┃ ┃ ┣ 📜plot_genes.Rmd: script para plotear los genes mediante R 
 ┃ ┣ 📂random: directorio para el analisis de los pseudo epitopos    
 ┃ ┃ ┣ 📜physcript.sh: script para lanzar el analisis de los pseudo epitopos    
 ┃ ┃ ┗ 📜randomize_epitopes.py: script de R para generar los pseudoepitopos  
 ┃ ┣ 📂RD_deletions: directorio para el analisis de epitopos que caen en Rds    
 ┃ ┃ ┣ 📜rd_coord.txt: ficero con las regiones rds    
 ┃ ┃ ┗ 📜scr.tcell.sh: script de bash con el comando awk, para apartir del fichero de las deleciones y los epitopos con las coordenadas, conocer que epitopos caen en estas regiones.  
 ┃ ┣ 📂tajima: directorio para el analisis de la D de tajima en epitopos  
 ┃ ┃ ┗ 📜tajima_bcell.R: script de R para calcular la D de tajima mediante un fasta como input    
 ┃ ┗ 📂treeTb: directorio para el analisis de filogenias en tuberculosis  
 ┃ ┃ ┗ 📜lanzar2.sh; script para lanzar la filogenia apartir de un alineamiento mediante IQ-Tree  
 ┗ 📂SARS_COV_2: directorio para los analisis en SARS-CoV-2    
 ┃ ┣ 📂alineamientos: directorio para generar los alineamientos de SARS-CoV-2    
 ┃ ┃ ┣ 📜chose_random.py: script para escoger secuencias al azar  
 ┃ ┃ ┣ 📜clean_aln.py: script para limpiar las secuencias de baja calidad  
 ┃ ┃ ┗ 📜run.sh: script para generar los alineamientos  
 ┃ ┣ 📂clusteres_epidemicos: analisis para obtener los clusters epidemicos  
 ┃ ┃ ┗ 📜tree.Rmd: script para contar los clusters de un arbiol filogenetico anotado  
 ┃ ┣ 📂gwas: analisis de asociacion de mutaciones con los valores de ct    
 ┃ ┃ ┣ 📜crea_matriz.py: script para crear una matriz de SNPs    
 ┃ ┃ ┣ 📜crea_pheno.py:: script para anotar fenotipicamente la matriz de snps  
 ┃ ┃ ┣ 📜gwas_pipe.sh: pipeline para realizar el analisis de gwas  
 ┃ ┃ ┣ 📜lanzar_gwas.sh: script para lanzar el analiis de gwas    
 ┃ ┃ ┣ 📜plotmanh.R: scrpit para plotear los snps mediante un manhattan  
 ┃ ┃ ┣ 📜ppca.R: scirpt para realizar un pca  
 ┃ ┃ ┣ 📜sacar_freq.py: script para sacar las frecuencias de las mutaciones    
 ┃ ┃ ┗ 📜transformar.R: script para transformar la matriz de los snps  
 ┃ ┣ 📂multivariable: analisis para realizar el analisis multivariable para los pacientes hospitalizados    
 ┃ ┃ ┣ 📜multivariate.Rmd: script de R para realizar el pca y los analisis univariables y multivariables    
 ┃ ┃ ┗ 📜plot_cts.Rmd: script para plotear los valores de Ct    
 ┃ ┣ 📂mutaciones: directorio para analizar las mutaciones en sars-cov-2  
 ┃ ┃ ┣ 📜anotar.py: script para anotar las mutaciones    
 ┃ ┃ ┣ 📜get_bases.py: scirpt para obtener las bases de una posicion determinada    
 ┃ ┃ ┣ 📜get_freq.py: script para obtener las frecuencias de las mutaciones de un alineamiento    
 ┃ ┃ ┗ 📜script_get.py: script para obtener un vcf a partir de un alineamiento  
 ┃ ┣ 📂phylo: directorio para las reconstrucciones filogeneticas en sars-cov-2    
 ┃ ┃ ┣ 📜cvd_all.sh: script para lanzar la filogenia en sars mediante IQ-tree  
 ┃ ┃ ┗ 📜mask_alignment.sh: script para enmascarar las posiciones problematicas de un alineamiento  

